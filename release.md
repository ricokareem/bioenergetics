# Bioenergetics Release Notes

## 1.0.1

* shorten app name & description
* xcode 7.2

## 1.0.0

* first release
* remove push notification
* remove iPad support
* fix icons

## 0.1.13

* splash screen

## 0.1.12

* added music and music on/off toggle
* cordova and ios updates
* support for iPhone 6 and 6s


## 0.1.9


* updated cordova ios platform


## 0.1.8


* Remove stopwatch on Ending Healing Session parts 1,2,3
* Do not perform Ending Healing Session for full 5 mins
* Pause/Reset video and timer if user navigates away during playback
* Replace the word 'site' with 'app' in Disclaimer
* Add Roland's pic to About Roland
