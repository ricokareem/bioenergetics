var videoSource = [];
var videoCount,
    cardsequence,
    $this_video,
    $this_title,
    $this_countdown,
    $demo,
    $stopwatch,
    n,
    i,
    cards,
    musicFlag;

function toggleMusic() {
    if (musicFlag) {
        $('#music_control a').html('<i class="fa fa-volume-off"></i>sound off</a>');
        $('#music_loop')[0].load();
        return musicFlag = false;
    } else {
        $('#music_control a').html('<i class="fa fa-volume-off"></i>sound on</a>');
        $('#music_loop')[0].play();
        return musicFlag = true;
    }
}

function setSequence(sequence) {
	var args = [].slice.call(arguments);
    n = 0;

    videoSource = [];
    cardsequence = [];
	args.forEach(function (arg) {
        videoSource[n] = "./media/" + arg + ".mp4";
        cardsequence[n] = arg;
        n++;
	});

    videoCount = videoSource.length;

}

function resetIndex () {
    i = 0;
}

function setupVideo(cardnum, i) {
    $this_video = $('#card_' + cardnum + ' ' + '.myVideo');
    $this_title = $('#card_' + cardnum + ' ' + 'h1');
    $this_countdown = $('#card_' + cardnum + ' ' + 'p.countdown');
    $this_video.attr('src', videoSource[0]);
    $this_video.get(0).load();

    switch (cardnum) {
    case 8:
        cardnum = 81;
        break;
    case 12:
        cardnum = 121;
        break;
    case 13:
        cardnum = 131;
        break;
    }

    if (cards[cardnum]) {
        $this_title.html(cards[cardnum + i].title);
    } else {
        $this_title.html('Step ' + (i + 1) + ': ' + cards[cardsequence[i]].title);
    }
    $this_countdown.html('Step ' + (i + 1) + ' of ' + n + ' - Perform this step for 5 minutes');

}


$(document).ready(function () {
    cards=$.parseJSON('{"1":    {        "id": "1",        "name": "card_1",        "title": "Balancing Energy Distribution",        "movie": "1",        "duration": "Perform this step for 5 minutes",        "showClock": true    },"2":    {        "id": "2",        "name": "card_2",        "title": "Opening and Repairing the Energy field: Knitting Movement",        "movie": "2",        "duration": "Perform this step for 5 minutes",        "showClock": true    },"3":    {        "id": "3",        "name": "card_3",        "title": "Energizing Sequence: Mobilization of Chi Energy",        "movie": "3",        "duration": "Perform this step for 5 minutes",        "showClock": true    },"4":    {        "id": "4",        "name": "card_4",        "title": "Scissors Movement",        "movie": "4",        "duration": "Perform this step for 5 minutes",        "showClock": true    },"5":    {        "id": "5",        "name": "card_5",        "title": "Pinch of Salt",        "movie": "5",        "duration": "Perform this step for 5 minutes",        "showClock": true    },"6":    {        "id": "6",        "name": "card_6",        "title": "Pressing Sequence",        "movie": "6",        "duration": "Perform this step for 5 minutes",        "showClock": true    },"7":    {        "id": "7",        "name": "card_7",        "title": "Spiral Pulling",        "movie": "7",        "duration": "Perform this step for 5 minutes",        "showClock": true    },"81":    {        "id": "81",        "name": "card_8",        "title": "Clockwise &amp; Counterclockwise Circular Motion: To Reset the Chi - part 1",        "movie": "81",        "duration": "Perform this step for 5 minutes",        "showClock": true    },"82":    {        "id": "82",        "name": "card_8",        "title": "Clockwise &amp; Counterclockwise Circular Motion: To Reset the Chi - part 2",        "movie": "82",        "duration": "Perform this step for 5 minutes",        "showClock": true    },"9":    {        "id": "9",        "name": "card_9",        "title": "Directing Energy Flow",        "movie": "9",        "duration": "Perform this step for 5 minutes",        "showClock": true    },"10":    {        "id": "10",        "name": "card_10",        "title": "Energy &quot;Flossing&quot;: Unblocking Energy Pathways",        "movie": "10",        "duration": "Perform this step for 5 minutes",        "showClock": true    },"11":    {        "id": "11",        "name": "card_11",        "title": "Energy Cocooning: Energy Firming or Shielding",        "movie": "11",        "duration": "Perform this step for 5 minutes",        "showClock": true    },"121":    {        "id": "121",        "name": "card_12",        "title": "Contact Healing - part 1",        "movie": "12",        "duration": "Perform this step for 5 minutes",        "showClock": true    },"122":    {        "id": "122",        "name": "card_12",        "title": "Contact Healing - part 2",        "movie": "122",        "duration": "Perform this step for 5 minutes",        "showClock": true    },"123":    {        "id": "123",        "name": "card_12",        "title": "Contact Healing - part 3",        "movie": "123",        "duration": "Perform this step for 5 minutes",        "showClock": true    },"131":    {        "id": "131",        "name": "card_13",        "title": "Ending the Healing Session - part 1",        "movie": "13",        "duration": "Perform this task only once.  You do not need to perform it for the full 5 minutes",        "showClock": false    },"132":    {        "id": "132",        "name": "card_13",        "title": "Ending the Healing Session - part 2",        "movie": "132",        "duration": "Perform this task only once.  You do not need to perform it for the full 5 minutes",        "showClock": false    },"133":    {        "id": "133",        "name": "card_13",        "title": "Ending the Healing Session - part 3",        "movie": "133",        "duration": "Perform this task only once.  You do not need to perform it for the full 5 minutes",        "showClock": false    }}');

    // SYMPTOMS AND CONDITIONS - CARDS

    $(".example").TimeCircles({ time: {
        Days: { show: false },
        Hours: { show: false },
        Minutes: { color: "#f57b00" },
        Seconds: { color: "#fc9829" }
    }, start: false});

    $stopwatch = $(".example.stopwatch");

    $(".start").on('click',function(e){
        e.preventDefault();
        $stopwatch.TimeCircles().start();
        $this_video.get(0).play();
        $(this).css('background', '#f57b00');
        $(this).css('color', '#fff');
        if (musicFlag == true) {
            $('#music_loop')[0].play();
        } else {
            $('#music_loop')[0].pause();
        }
    });
    $(".stop").on('click',function(e){
        e.preventDefault();
        $stopwatch.TimeCircles().stop();
        $this_video.get(0).pause();
        $('#music_loop')[0].pause();
        $(this).css('background', '#f57b00');
        $(this).css('color', '#fff');
    });
    $(".restart").on('click',function(e){
        e.preventDefault();
        $stopwatch.TimeCircles().restart().stop();
        $('#music_loop')[0].pause();
        $this_video.get(0).load();
        $(this).css({
            'background':'#f57b00',
            'color':'#fff'
        });
    });
    $(".backward").on('click',function(e){
        e.preventDefault();
        $stopwatch.TimeCircles().restart().stop();
        i--;

        if (i <= 0) { i = 0; }

        if (!cards[cardsequence[i]].showClock) {
            $('.stopwatch').hide();
        } else {
            $('.stopwatch').show();
        }

        $this_title.html('Step ' + (i+1) + ': ' + cards[cardsequence[i]].title);
        $this_countdown.html('Step ' + (i+1) +' of '+ n + ' - ' + cards[cardsequence[i]].duration);

        $this_video.attr('src', videoSource[i]);
        $this_video.get(0).load();
        $('#music_loop')[0].pause();
        $(this).css({
            'background':'#f57b00',
            'color':'#fff'
        });
    });
    $(".skip").on('click',function(e){
        e.preventDefault();
        $stopwatch.TimeCircles().restart().stop();
        i++;
        if (i >= (videoCount)) { i = 0; }

        if (!cards[cardsequence[i]].showClock) {
            $('.stopwatch').hide();
        } else {
            $('.stopwatch').show();
        }

        $this_title.html('Step ' + (i+1) + ': ' + cards[cardsequence[i]].title);
        $this_countdown.html('Step ' + (i+1) +' of '+ n + ' - ' + cards[cardsequence[i]].duration);

        $this_video.attr('src', videoSource[i]);
        $this_video.get(0).load();
        $('#music_loop')[0].pause();
        $(this).css({
            'background':'#f57b00',
            'color':'#fff'
        });
    });
    // PAUSE VIDEO IF NAVIGATE AWAY FROM PAGE
    $('header, footer').on('click', function(){
        $this_video.get(0).pause();
        $('#music_loop')[0].pause();
        $stopwatch.TimeCircles().restart().stop();
    })

    // HEALING SEQUENCES - DEMOS
    $demo = $('.myVideo.demos', '#movie');
    $demo.bind('ended', function( myHandler) {
        i++;
        if (i == (videoCount)) { i = 0; }

        $this_title.html(cards[cardsequence[i]].title);
        $this_video.attr('src', videoSource[i]);
        $this_video.get(0).load().play();
    });

    FastClick.attach(document.body);

    musicFlag = true;
});
