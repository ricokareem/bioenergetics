module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
    // uglify: {
    //   options: {
    //     banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
    //   },
    //   build: {
    //     src: 'src/<%= pkg.name %>.js',
    //     dest: 'build/<%= pkg.name %>.min.js'
    //   }
    // },
        jade: {
            compile: {
                options: {
                    data: {
                        debug: false
                    }
                },
                files: {
                    "www/index.html": ["views/index.jade"]
                    // "platforms/ios/www/index.html": ["views/index.jade"]
                }
            }
        },
        less: {
            options: {
                paths: ['assets']
            },
            // target name
            src: {
                // no need for files, the config below should work
                expand: true,
                cwd:    "assets",
                src:    "*.less",
                dest:   "www/css",
                // dest:   "platforms/ios/www/css",
                ext:    ".css"
            }
        },
        watch: {
            css: {
                files: '**/*.less',
                tasks: ['less']
            },
            html: {
                files: '**/*.jade',
                tasks: ['jade']
            }
        }
    });

    // // Load the plugin that provides the "uglify" task.
    // grunt.loadNpmTasks('grunt-contrib-uglify');

    // JADE
    grunt.loadNpmTasks('grunt-contrib-jade');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task(s).
    grunt.registerTask('default', ['jade','less','watch']);

};
